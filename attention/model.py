#!/usr/bin/env python

import logging
import torch
import torch.nn as nn
from torch import optim
from torch.optim import lr_scheduler
import numpy as np
from attention.network import TNT
from sklearn.preprocessing import StandardScaler
import matplotlib as plt

import os
import string
import random

from attention.util import timer

class AttnModel():
    def __init__(self, X, y, T,
                 input_size, output_size, num_channels, kernel_size, dropout,
                 batch_size,
                 learning_rate,
                 epochs,
                 path,
                 incremental,
                 shuffle = True,
                 device = "cpu"):
        super(AttnModel, self).__init__()
        self.device = device
        self.input_size = input_size
        self.output_size = output_size
        self.num_channels = num_channels
        self.kernel_size = kernel_size
        self.dropout = dropout
        self.learning_rate = learning_rate
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.epochs = epochs
        self.T = T
        (self.X, X_scaler) = self.rescale(X)
        (self.y, y_scaler) = self.rescale(y)
        self.X = self.reshapeData(self.X).permute(0, 2, 1)
        self.y = self.reshapeData(self.y)[:, -1].reshape(-1, 1)

        self.tnt = TNT(self.input_size, self.output_size, self.num_channels, self.kernel_size, self.dropout,
                       device=self.device)

        # Loss function
        self.criterion = nn.MSELoss()

        self.optimizer = optim.Adam(params=self.tnt.parameters(), lr=self.learning_rate)
        self.scheduler = lr_scheduler.ReduceLROnPlateau(self.optimizer, factor=0.1, patience=5)

        # Training set
        self.train_timesteps = int(self.X.shape[0] * 0.7)
        self.test_timesteps = int(self.X.shape[0] * 0.85) - self.train_timesteps
        self.validation_timesteps = self.X.shape[0] - self.train_timesteps - self.test_timesteps

        self.bs_train = np.arange(0, self.train_timesteps, self.batch_size)
        self.bs_test = np.arange(self.train_timesteps, self.train_timesteps + self.test_timesteps, self.batch_size)
        self.bs_validation = np.arange(self.train_timesteps + self.test_timesteps, self.X.shape[0], self.batch_size)
        self.iter_losses = []
        self.epoch_losses = []
        self.test_loss = []
        self.modelId = None
        self.path = path
        self.incremental = incremental

    def ran_gen(self, size, chars= string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for x in range(size))

    @timer.timed
    def saveModel(self, epochId):
        self.modelId = self.ran_gen(16) if self.modelId == None else self.modelId + "_" + str(epochId)
        file = self.path + "/" + self.modelId + ".pth"
        logging.info(f"Saving the model at {file}")
        checkpoint = {}
        checkpoint.update({"input_size": self.input_size})
        checkpoint.update({"output_size": self.output_size})
        checkpoint.update({"num_channels": self.num_channels})
        checkpoint.update({"kernel_size": self.kernel_size})
        checkpoint.update({"dropout": self.dropout})
        checkpoint.update({"device": self.device})
        checkpoint.update({"state_dict": self.tnt.state_dict()})
        torch.save(checkpoint, file)

    @timer.timed
    def loadModel(self):
        file = self.path + "/" + self.modelId + ".pth"
        if(os.path.isfile(file)):
            logging.info(f"Loading the model at {file}")
            checkpoint = torch.load(file)
            self.input_size = checkpoint["input_size"]
            self.output_size = checkpoint["output_size"]
            self.num_channels = checkpoint["num_channels"]
            self.kernel_size = checkpoint["kernel_size"]
            self.dropout = checkpoint["dropout"]
            self.device = checkpoint["device"]
            self.tnt = TNT(self.input_size, self.output_size, self.num_channels, self.kernel_size, self.dropout,
                       device=self.device)
            state_dict = checkpoint["state_dict"]
            self.tnt.load_state_dict(state_dict)
        else:
            logging.warning(f"Model {file} does not exist")


    def reshapeData(self, data, overlapping=True):
        reshapedData = []
        if (overlapping):
            idx = 0
            while (idx < data.shape[0]):
                start = idx
                end = idx + self.T
                idx = idx + 1
                reshapedData.append(torch.tensor(data[start:end], dtype=torch.float32))
                if (end >= data.shape[0]):
                    break
        else:
            idx = 0
            while (idx < data.shape[0]):
                start = idx
                end = idx + self.T
                idx = end
                reshapedData.append(torch.tensor(data[start:end], dtype=torch.float32))
                if (end >= data.shape[0]):
                    break
        reshapedData = torch.stack(reshapedData)
        return reshapedData

    def rescale(self, data):
        scaler = StandardScaler()
        data = scaler.fit_transform(data)
        return (data, scaler)

    def earlyStop(self, currentEpoch):
        threeP = self.epochs * 0.3
        if(currentEpoch > threeP):
            losses = np.array(self.test_loss)
            if(np.std(losses) < 0.0001):
                return True
            else:
                return False
        else:
            return False

    @timer.timed
    def train(self):
        if(self.incremental == True):
            self.loadModel()

        self.tnt.train()
        """training process."""
        best_loss = None
        for epoch in range(self.epochs):
            if self.shuffle:
                ref_idx = np.random.permutation(self.bs_train)
            else:
                ref_idx = self.bs_train

            for idx in ref_idx:
                x = self.X[idx:idx + self.batch_size]
                y = self.y[idx:idx + self.batch_size]

                self.optimizer.zero_grad()
                logging.debug(f"Training Batch - {idx} Input - {x.shape}")
                y_pred = self.tnt(x)
                logging.debug(f"Training Batch - {idx} Output Pred - {y_pred.shape}")
                logging.debug(f"Training Batch - {idx} Output True - {y.shape}")
                loss = self.criterion(y_pred, y)
                self.iter_losses.append(loss.item())
                loss.backward()
                self.optimizer.step()
                if(best_loss == None):
                    best_loss = np.array(self.iter_losses).min()
                if(loss < best_loss):
                    self.saveModel(epoch)

            avg_test_loss = self.evaluate(self.bs_test)
            self.scheduler.step(avg_test_loss)
            self.test_loss.append(avg_test_loss)

            avg_iter_loss = np.mean(self.iter_losses[-self.batch_size:])
            self.epoch_losses.append(avg_iter_loss)

            grad_max = 0.0
            grad_means = 0.0
            grad_count = 0
            for p in self.tnt.parameters():
                grad_max = max(grad_max, p.grad.abs().max().item())
                grad_means += (p.grad ** 2).mean().sqrt().item()
                grad_count += 1

            grad_l2 = grad_means / grad_count
            logging.info(f"grad_l2 - {grad_l2}")
            logging.info(f"grad_max - {grad_max}")
            logging.info(f"parameters - {grad_count}")

            logging.info(f"Avg Iter Loss: {avg_iter_loss}")
            logging.info(f"Avg Test Loss: {avg_test_loss}")
            logging.info(f"Epochs: {epoch} Train Loss: {self.epoch_losses[epoch]}")

            self.incremental = False
            if(self.earlyStop(epoch)):
                logging.info(f"Early Stopping {epoch}")
                self.saveModel(str(epoch) + "better")
                break

        return self.evaluate(self.bs_test)

    @timer.timed
    def evaluate(self, bs, savedModel = None, path = None):
        if(savedModel):
            self.loadModel(savedModel, path)

        self.tnt.eval()
        losses = []
        for idx in bs:
            x = self.X[idx:idx + self.batch_size]
            y = self.y[idx:idx + self.batch_size]
            with torch.no_grad():
                y_hat = self.tnt(x)
                loss = self.criterion(y_hat, y)
                losses.append(loss.item())

        avg_loss = np.mean(np.array(losses))
        return avg_loss

    @timer.timed
    def verify(self, savedModel = None, path = None):
        if(savedModel):
            self.loadModel(savedModel, path)

        self.tnt.eval()
        losses = []
        y_train_pred = []
        y_test_pred = []
        y_validation_pred = []
        for idx in self.bs_train:
            x = self.X[idx:idx + self.batch_size]
            y = self.y[idx:idx + self.batch_size]
            with torch.no_grad():
                y_hat = self.tnt(x)
                loss = self.criterion(y_hat, y)
                losses.append(loss.item())
                y_train_pred.extend(y_hat.detach().numpy().tolist())
        for idx in self.bs_test:
            x = self.X[idx:idx + self.batch_size]
            y = self.y[idx:idx + self.batch_size]
            with torch.no_grad():
                y_hat = self.tnt(x)
                loss = self.criterion(y_hat, y)
                losses.append(loss.item())
                y_test_pred.extend(y_hat.detach().numpy().tolist())
        for idx in self.bs_validation:
            x = self.X[idx:idx + self.batch_size]
            y = self.y[idx:idx + self.batch_size]
            with torch.no_grad():
                y_hat = self.tnt(x)
                loss = self.criterion(y_hat, y)
                losses.append(loss.item())
                y_validation_pred.extend(y_hat.detach().numpy().tolist())

        y = self.y_scaler.inverse_transform(self.y.numpy().reshape(-1, 1)).reshape(-1)
        y_train_pred = self.y_scaler.inverse_transform(y_train_pred.reshape(-1, 1)).reshape(-1)
        y_test_pred = self.y_scaler.inverse_transform(y_test_pred.reshape(-1, 1)).reshape(-1)
        y_validation_pred = self.y_scaler.inverse_transform(y_validation_pred.reshape(-1, 1)).reshape(-1)

        plt.plot(range(1, 1 + y.shape[0]), y, label="True")

        plt.plot(range(1, len(y_train_pred) + 1), y_train_pred, label='Predicted - Train')

        plt.plot(range(self.train_timesteps + 1, self.train_timesteps + len(y_test_pred) + 1),
                 y_test_pred, label='Predicted - Test')

        plt.plot(range(self.train_timesteps + self.test_timesteps + 1, len(self.y) + 1), y_validation_pred, label='Predicted - Test')

        plt.legend(loc='upper left')
        plt.show()
