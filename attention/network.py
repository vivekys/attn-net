#!/usr/bin/env python

import logging
import torch.nn as nn
from attention.tcn.blocks import TemporalAtnConvNet

class TNT(nn.Module):
    def __init__(self, input_size, output_size, num_channels, kernel_size, dropout, device='cpu'):
        super(TNT, self).__init__()
        self.device = device
        self.tcn = TemporalAtnConvNet(input_size, num_channels, kernel_size=kernel_size, dropout=dropout,
                                      device=self.device)
        self.linear = nn.Linear(num_channels[-1], output_size)

    def forward(self, x):
        logging.debug(f"TNT Input - {x.shape}")
        y1 = self.tcn(x.to(self.device))
        output = self.linear(y1[:, :, -1])
        logging.debug(f"TNT Output - {output.shape}")
        return output
