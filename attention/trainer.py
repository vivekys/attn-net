#!/usr/bin/env python

import logging
import os
import torch
import numpy as np
import pandas as pd
from attention.model import AttnModel

from types import SimpleNamespace
from sklearn.preprocessing import StandardScaler
from hyperopt import fmin, tpe, hp, Trials, STATUS_OK, STATUS_FAIL

def read_data(input_path, size="Small"):
    """Read nasdaq stocks data.

    Args:
        input_path (str): directory to nasdaq dataset.

    Returns:
        X (np.ndarray): features.
        y (np.ndarray): ground truth.

    """
    df = pd.read_csv(input_path)
    X = df.iloc[:, 0:-1]
    y = df.iloc[:, -1]
    length = X.shape[0]
    if(size == "Tiny"):
        length = length * 0.1
    elif(size == "Small"):
        length = length * 0.3
    elif(size == "Medium"):
        length = length * 0.5
    else:
        None
    length = int(length)
    return X.iloc[:length].values, y[:length].values.reshape(-1, 1)


def run(space):
    opt = {}
    opt.update({'dataroot': "data/nasdaq100_padding.csv"})
    opt.update({'workers': 2})
    opt.update({'batchsize': 32})
    opt.update({'ntimestep': 32})
    opt.update({'epochs': 50})
    opt.update({'lr': 0.01})

    opt = SimpleNamespace(**opt)
    print(space)
    # Read dataset
    X, y = read_data(opt.dataroot, size="Tiny")

    channel_sizes = [int(space['num_hidden_channels'])] * int(np.ceil(np.log2(space['ntimestep'])))

    model = AttnModel(X= X, y = y, T = int(space['ntimestep']), input_size = X.shape[1], output_size = 1,
                      num_channels = channel_sizes, kernel_size = int(space['kernel']), dropout = space['dropout'],
                      batch_size = int(space['batchSize']), learning_rate = space['lr'], epochs = int(space['epoch']),
                      path = os.getcwd() + "/saved", incremental = None, device="cpu")

    # Train
    loss = model.train()
    result = {
        "loss": loss,
        "status": STATUS_OK,
        "id": model.modelId
    }
    return result


def run_trials(hyperparam_generators):
    """
    Run a number of trials, save after a defined number of iterations.
    """
    trials_step = 1  # how many additional trials to do after loading saved trials. 1 = save after iteration
    max_evals = 20  # initial max_evals. put something small to not have to wait

    try:
        trials = torch.load('params.hyperopt.pth')
        max_evals = len(trials.trials) + trials_step
        print('Continuing from {} trials to {} (+{}) trials'.format(len(trials.trials), max_evals, trials_step))
    except:
        trials = Trials()
        print('Starting new trials...')

    best = fmin(fn=run,
                space=hyperparam_generators,
                algo=tpe.suggest,
                trials=trials,
                max_evals=max_evals)

    print("Best:", trials.best_trial)

    # save the trials object
    torch.save(trials, "params.hyperopt.pth")
    return trials

if __name__ == '__main__':
    os.chdir("/Users/vivek/myCode/company/i18r/attn-net")

    log_format = '%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s'
    log_datefmt = '%Y-%m-%d:%H:%M:%S'
    logging.basicConfig(filename="logs/TNT.log", format=log_format, datefmt=log_datefmt, level=logging.DEBUG)

    all_hyperparam_generators = {
        'epoch': hp.quniform('epoch', 20, 100, 1),
        'dropout': hp.lognormal('dropout', -2.0, 0.1),
        'kernel': hp.quniform('kernel', 3, 9, 1),
        'num_hidden_channels': hp.quniform('num_hidden_channels', 30, 120, 1),
        'lr': hp.lognormal('lr', -4.0, 0.6),
        'ntimestep': hp.quniform('ntimestep', 10, 30, 1),
        'batchSize': hp.choice('batchSize', [32, 64, 128, 256])
    }
    simple = {
        'epoch': hp.choice('epoch', [2]),
        'dropout': hp.lognormal('dropout', -2.0, 0.1),
        'kernel': hp.quniform('kernel', 3, 9, 1),
        'num_hidden_channels': hp.choice('num_hidden_channels', [3]),
        'lr': hp.lognormal('lr', -4.0, 0.6),
        'ntimestep': hp.choice('ntimestep', [10]),
        'batchSize': hp.choice('batchSize', [128])
    }
    while True:
        run_trials(simple)
